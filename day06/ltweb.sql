CREATE DATABASE IF NOT EXISTS ltweb;
USE ltweb;

CREATE TABLE `students`
(
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    gender VARCHAR(10) NOT NULL,
    department VARCHAR(100) NOT NULL,
    birthdate DATE NOT NULL,
    address VARCHAR(255),
    img_path VARCHAR(255)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Đăng ký Tân sinh viên</title>
    <style>
        body {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
        }

        .container {
            display: inline;
            flex-direction: column;
            align-items: center;
            margin: 0 auto;
            background-color: white;
            text-align: left;
            border: 2px solid #005b8b;
            padding: 64px 45px 34px 35px;
        }

        label[for="name"],
        label[for="department"],
        label[for="gender"],
        label[for="birthdate"],
        label[for="address"],
        label[for="infor"] {
            width: 96px;
            height: 35px;
            border: 2px solid #497B8D;
            display: inline-block;
            color: white;
            background-color: #70AD47;
            text-align: center;
            line-height: 32px;
        }

        input[name="name"],
        input[name="address"] {
            width: 265px;
            height: 32px;
            border: 2px solid #41719C;
            margin-left: 10px;
            padding: 2px;
        }

        input[name="img"] {
            width: 265px;
            height: 32px;
            margin-left: 10px;
            padding: 2px;
        }

        select[name="department"], input[name="birthdate"] {
            width: 145px;
            height: 36px;
            border: 2px solid #41719C;
            margin-left: 10px;
        }

        button {
            width: 130px;
            height: 43px;
            color: white;
            background-color: #70AD47;
            margin-top: 130px;
            margin-left: 130px;
            border-radius: 10px;
            border: 2px solid #005b8b;
        }

        button:hover {
            cursor: pointer;
            background-color: #005b8b;
        }

        input[type="radio"] {
            width: 20px;
            height: 20px;
            margin: -3px 16px 0px -33px;
            vertical-align: middle;
        }

        .data {
            display: inline-block;
            width: 265px;
            height: 32px;
            margin-left: 10px;
        }

        .avatar {
            width: 150px;
            height: 150px;
            object-fit: cover;
            border: 2px solid #fff;
            margin-left: 10px;
            transform: translateY(129px);
            margin-top: -133px;
        }

    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
</head>
<body>
<div class="container">
    <h2>Thông tin đăng ký sinh viên</h2>
    <form method="post" enctype="multipart/form-data">
        <label for="name">Họ và tên</label>
        <div class="data"><?php echo isset($_POST['name']) ? $_POST['name'] : ''; ?></div>
        <br><br>

        <label for="gender">Giới tính</label>
        <div class="data"><?php echo isset($_POST['gender']) ? ($_POST['gender'] == 0 ? 'Nam' : 'Nữ') : ''; ?></div>
        <br><br>

        <label for="birthdate">Ngày sinh</label>
        <div class="data">
            <?php echo isset($_POST['day']) && isset($_POST['month']) && isset($_POST['year']) ? $_POST['day'] . '/' . $_POST['month'] . '/' . $_POST['year'] : ''; ?>
        </div>
        <br><br>

        <label for="address">Địa chỉ</label>
        <div class="data"><?php echo isset($_POST['city']) && isset($_POST['district']) ? $_POST['district'] . ' - ' . ($_POST['city'] == 1 ? 'Hà Nội' : 'Tp. Hồ Chí Minh') : ''; ?></div>
        <br><br>

        <label for="infor">Thông tin khác</label>
        <div class="data"><?php echo isset($_POST['infor']) ? $_POST['infor'] : ''; ?></div>
        <br><br>
    </form>
</div>
</body>
</html>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Đăng ký Tân sinh viên</title>
    <style>
        body {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
        }

        .container {
            display: inline;
            flex-direction: column;
            align-items: center;
            margin: 0 auto;
            background-color: white;
            text-align: left;
            border: 2px solid #005b8b;
            padding: 64px 45px 34px 35px;
        }

        label[for="name"],
        label[for="gender"],
        label[for="birthdate"],
        label[for="address"],
        label[for="infor"]{
            width: 96px;
            height: 35px;
            border: 2px solid #497B8D;
            display: inline-block;
            color: white;
            background-color: #70AD47;
            text-align: center;
            line-height: 32px;
        }

        .radio-inline {
            padding-left: 35px;
            position: relative;
            margin: 0;
            line-height: 20px;

        }

        input[name="name"],
        input[name="address"] {
            width: 265px;
            height: 32px;
            border: 2px solid #41719C;
            margin-left: 10px;
            padding: 2px;
        }

        input[name="img"] {
            width: 265px;
            height: 32px;
            margin-left: 10px;
            padding: 2px;
        }

        select[name="department"], input[name="birthdate"] {
            width: 145px;
            height: 36px;
            border: 2px solid #41719C;
            margin-left: 10px;
        }

        button {
            width: 130px;
            height: 43px;
            color: white;
            background-color: #70AD47;
            margin-top: 40px;
            margin-left: 130px;
            border-radius: 10px;
            border: 2px solid #005b8b;
        }

        button:hover {
            cursor: pointer;
            background-color: #005b8b;
        }

        input[type="radio"] {
            width: 20px;
            height: 20px;
            margin: -3px 16px 0px -33px;
            vertical-align: middle;
        }

        .radio-inline {
            margin-left: 10px;
        }


        #error-container {
            /*min-height: 20px;*/
            color: red;
            margin-bottom: 10px;
        }

    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
</head>
<body>

<div class="container">
    <form method="post" action="regist_student.php" enctype="multipart/form-data">

        <h2> Form Đăng ký sinh viên</h2>

        <div id="error-container"></div>
        <br><br>

        <label for="name" class="required-label">Họ và tên</label>
        <input type="text" name="name" id="name" placeholder="Nhập họ và tên">
        <br><br>

        <label class="required-label" for="gender">Giới tính</label>
        <?php
        $genders = [
            0 => 'Nam',
            1 => 'Nữ',];

        for ($i = 0; $i < count($genders); $i++) {
            echo '<label class="radio-inline">';
            echo "<input type='radio' class='form-control' name='gender' value='$i'>$genders[$i]</label>";
        }
        ?>
        <br><br>

        <label class="required-label" for="birthdate">Ngày sinh</label>
        <select name="day" id="day">
            <option value=""></option> <!-- Giá trị rỗng -->
            <?php
            // Tạo danh sách ngày từ 01 đến 31
            for ($i = 1; $i <= 31; $i++) {
                $day = sprintf("%02d", $i);
                echo "<option value='$day'>$day</option>";
            }
            ?>
        </select>

        <select name="month" id="month">
            <option value=""></option> <!-- Giá trị rỗng -->
            <?php
            // Tạo danh sách tháng từ 01 đến 12
            for ($i = 1; $i <= 12; $i++) {
                $month = sprintf("%02d", $i);
                echo "<option value='$month'>$month</option>";
            }
            ?>
        </select>

        <select name="year" id="year">
            <option value=""></option> <!-- Giá trị rỗng -->
            <?php
            $currentYear = date("Y");
            $startYear = $currentYear - 40;
            $endYear = $currentYear - 15;

            // Tạo danh sách năm sinh từ "Năm hiện tại - 40" đến "Năm hiện tại - 15"
            for ($i = $startYear; $i <= $endYear; $i++) {
                echo "<option value='$i'>$i</option>";
            }
            ?>
        </select>
        <br><br>

        <label for="address">Địa chỉ</label>
        <select name="city" id="city">
            <option value=""></option>
            <option value="1">Hà Nội</option>
            <option value="2">Tp. Hồ Chí Minh</option>
        </select>
        <select name="district" id="district">
            <option value=""></option>
        </select>
        <br><br>

        <label for="infor">Thông tin khác</label>
        <textarea type="text" id="infor" name="infor" rows="4"></textarea>
        <br><br>

        <button type="submit" id="registerButton">Đăng ký</button>
    </form>
</div>

<script>


    // Xử lý quận
    // Danh sách quận tương ứng với Hà Nội và Tp. Hồ Chí Minh
    var districts = {
        "1": ["Hoàng Mai", "Thanh Trì", "Nam Từ Liêm", "Hà Đông", "Cầu Giấy"],
        "2": ["Quận 1", "Quận 2", "Quận 3", "Quận 7", "Quận 9"]
    };

    var citySelect = document.getElementById("city");
    var districtSelect = document.getElementById("district");

    // Hàm để cập nhật danh sách quận khi thành phố được chọn
    citySelect.addEventListener("change", function() {
        var cityId = this.value;
        districtSelect.innerHTML = ""; // Xóa tất cả các option trong select box quận

        // Thêm option trống vào đầu tiên
        var defaultOption = document.createElement("option");
        defaultOption.value = "";
        districtSelect.appendChild(defaultOption);

        // Thêm các option quận tương ứng vào select box quận
        for (var i = 0; i < districts[cityId].length; i++) {
            var option = document.createElement("option");
            option.value = districts[cityId][i];
            option.text = districts[cityId][i];
            districtSelect.appendChild(option);
        }
    });

    $(document).ready(function () {
        $('#registerButton').click(function (event) {
            // Reset previous error messages
            $('#error-container').empty();

            var errorMessage = '';

            // Validate name field
            var name = $('#name').val();
            var regName = /^[a-zA-Z\s]+$/;
            if (name === '') {
                errorMessage += 'Hãy nhập họ và tên.<br>';
            } else if (!regName.test(name)) {
                errorMessage += 'Họ tên không chứa các kí tự đặc biệt hoặc số.<br>';
            }

            // Validate gender field
            var gender = $('input[name="gender"]:checked').val();
            if (gender === undefined) {
                errorMessage += 'Hãy chọn giới tính.<br>';
            }

            // Validate day, month, and year fields for birthdate
            var day = $('#day').val();
            var month = $('#month').val();
            var year = $('#year').val();
            if (day === '' || month === '' || year === '') {
                errorMessage += 'Hãy chọn ngày sinh.<br>';
            } else {
                var regBirthdate = /^\d{4}-\d{2}-\d{2}$/;
                if (!regBirthdate.test(year + '-' + month + '-' + day)) {
                    errorMessage += 'Ngày sinh không hợp lệ.<br>';
                }
            }

            // Validate city and district fields
            var city = $('#city').val();
            var district = $('#district').val();
            if (city === '' || district === '') {
                errorMessage += 'Hãy chọn địa chỉ.<br>';
            }

            // Display error message or show success alert
            if (errorMessage !== '') {
                event.preventDefault();
                $('#error-container').html(errorMessage);
            }
        });
    });

</script>

</body>
</html>

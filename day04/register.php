<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Đăng ký Tân sinh viên</title>
    <style>
        body {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
        }

        .container {
            display: inline;
            flex-direction: column;
            align-items: center;
            margin: 0 auto;
            background-color: white;
            text-align: left;
            border: 2px solid #005b8b;
            padding: 64px 45px 34px 35px;
        }

        label[for="name"],
        label[for="department"],
        label[for="gender"],
        label[for="birthdate"],
        label[for="address"] {
            width: 96px;
            height: 35px;
            border: 2px solid #497B8D;
            display: inline-block;
            color: white;
            background-color: #70AD47;
            text-align: center;
            line-height: 32px;
        }

        .radio-inline {
            padding-left: 35px;
            position: relative;
            margin: 0;
            line-height: 20px;

        }

        input[name="name"] {
            width: 265px;
            height: 32px;
            border: 2px solid #41719C;
            margin-left: 10px;
        }

        select[name="department"], input[name="birthdate"] {
            width: 145px;
            height: 36px;
            border: 2px solid #41719C;
            margin-left: 10px;
        }

        textarea[name="address"] {
            width: 265px;
            height: 73px;
            border: 2px solid #41719C;
            margin-left: 10px;
            transform: translate(1px, 56px);
            margin-top: -61px;
            resize: none;
        }

        button {
            width: 130px;
            height: 43px;
            color: white;
            background-color: #70AD47;
            margin-top: 40px;
            margin-left: 130px;
            border-radius: 10px;
            border: 2px solid #005b8b;
        }

        button:hover {
            cursor: pointer;
            background-color: #005b8b;
        }

        input[type="radio"] {
            width: 20px;
            height: 20px;
            margin: -3px 16px 0px -33px;
            vertical-align: middle;
        }

        .radio-inline {
            margin-left: 10px;
        }

        .required-label::after {
            content: " *";
            color: red;
        }

        #error-container {
            /*min-height: 20px;*/
            color: red;
            margin-bottom: 10px;
        }

    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
</head>
<body>

<div class="container">
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
        <div id="error-container"></div>

        <label class="required-label" for="name">Họ và tên</label>
        <input type="text" id="name" name="name" required><br><br>

        <label class="required-label" for="gender">Giới tính</label>
        <?php
        $genders = [
            0 => 'Nam',
            1 => 'Nữ',
        ];

        for ($i = 0; $i < count($genders); $i++) {
            echo '<label class="radio-inline">';
            echo "<input type='radio' class='form-control' name='gender' value='$i'>$genders[$i]</label>";
        }
        ?><br><br>

        <label class="required-label" for="department">Phân khoa</label>
        <select id="department" name="department" required>
            <option value="">--Chọn phân khoa--</option>
            <?php
            $departments = [
                'MAT' => 'Khoa học máy tính',
                'KDL' => 'Khoa học vật liệu',
            ];

            foreach ($departments as $key => $value) {
                echo "<option value='$key'>$value</option>";
            }
            ?>
        </select><br><br>

        <label class="required-label" for="birthdate">Ngày sinh</label>
        <input type="date" id="birthdate" name="birthdate" required><br><br>

        <label for="address">Địa chỉ</label>
        <textarea type="text" id="address" name="address" rows="4"></textarea><br><br>

        <button type="submit" id="registerButton">Đăng ký</button>
    </form>
</div>

<script>
    $(document).ready(function () {
        $('#registerButton').click(function (event) {
            // Reset previous error messages
            $('#error-container').empty();

            var errorMessage = '';

            // Validate name field
            var name = $('#name').val();
            var regName = /^[a-zA-Z\s]+$/;
            if (name === '') {
                errorMessage += 'Hãy nhập họ và tên.<br>';
            } else if (!regName.test(name)) {
                errorMessage += 'Họ tên không chứa các kí tự đặc biệt hoặc số.<br>';
            }

            // Validate gender field
            var gender = $('input[name="gender"]:checked').val();
            if (gender === undefined) {
                errorMessage += 'Hãy chọn giới tính.<br>';
            }

            // Validate department field
            var department = $('#department').val();
            if (department === '') {
                errorMessage += 'Hãy chọn phân khoa.<br>';
            }

            // Validate birthdate field
            var birthdate = $('#birthdate').val();
            if (birthdate === '') {
                errorMessage += 'Hãy chọn ngày sinh.<br>';
            } else {
                var regBirthdate = /^\d{4}-\d{2}-\d{2}$/;
                if (!regBirthdate.test(birthdate)) {
                    errorMessage += 'Ngày sinh không hợp lệ.<br>';
                }
            }

            // Display error message
            if (errorMessage !== '') {
                event.preventDefault();
                $('#error-container').html(errorMessage);
            } else {
                alert('Đăng ký thành công!');
            }
        });
    });
</script>

</body>
</html>

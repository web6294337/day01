<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <style>
        body {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
        }

        .container {
            width: 500px;
            height: 210px;
            border: 2px solid #005b8b;
            margin: 0 auto;
            text-align: center;
            padding: 10px;
            background-color: white;
        }

        p {
            font-weight: lighter ;
            margin: 0 auto;
            max-width: 363px;
            font-weight: bold;
            text-align: left;
            background-color: #E0E0E0;
            padding: 10px;
            margin-bottom: 10px;
            margin-top: 10px;
        }

        label {
            display: inline-block;
            width: 150px;
            color: white;
            text-align: left;
            height: 22px;
            background-color: #0077b6;
            padding: 5px;
            border: 1px solid #005b8b;
            margin-bottom: 9px;
        }

        input {
            width: 200px;
            height: 29px;
            margin-bottom: 10px;
            margin-left: 10px;
            border: 2px solid #005b8b;
        }

        button {
            width: 130px;
            height: 43px;
            color: white;
            background-color: #0077b6;
            margin-bottom: 20px;
            margin-top: 10px;
            border-radius: 10px;
        }
    </style>
</head>

<body>

<div class="container">
    <p>
        <?php
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $timestamp = time();

        $daysInVietnamese = [
            'Monday' => 'thứ 2',
            'Tuesday' => 'thứ 3',
            'Wednesday' => 'thứ 4',
            'Thursday' => 'thứ 5',
            'Friday' => 'thứ 6',
            'Saturday' => 'thứ 7',
            'Sunday' => 'chủ nhật'
        ];

        $dayOfWeek = date('l', $timestamp);
        $dayInVietnamese = $daysInVietnamese[$dayOfWeek];
        echo "Bây giờ là: " . date('H:i', $timestamp) . ", $dayInVietnamese ngày " . date('d/m/Y', $timestamp);
        ?>
    </p>
    <form method="post" action="process_login.php">
        <label for="username">Tên đăng nhập</label>
        <input type="text" id="username" name="username" required>

        <label for="password">Mật khẩu</label>
        <input type="password" id="password" name="password" required>

        <button type="submit">Đăng Nhập</button>
    </form>
</div>

</body>

</html>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Đăng ký Tân sinh viên</title>
    <style>
        body {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
        }

        .container {
            display: inline;
            flex-direction: column;
            align-items: center;
            margin: 0 auto;
            background-color: white;
            text-align: left;
            border: 2px solid #005b8b;
            padding: 64px 45px 34px 35px;
        }

        label[for="name"],
        label[for="department"],
        label[for="gender"] {
            width: 96px;
            height: 35px;
            border: 1px solid #005b8b;
            display: inline-block;
            color: white;
            background-color: #0077b6;
            text-align: center;
            line-height: 32px;
        }

        .radio-inline {
            padding-left: 35px;
            position: relative;
            margin: 0;
            line-height: 20px;

        }

        input[name="name"] {
            width: 265px;
            height: 32px;
            border: 2px solid #41719C;
            margin-left: 10px;
        }

        select[name="department"] {
            width: 145px;
            height: 36px;
            border: 2px solid #41719C;
            margin-left: 10px;
        }

        button {
            width: 130px;
            height: 43px;
            color: white;
            background-color: #70AD47;
            margin-top: 20px;
            margin-left: 130px;
            border-radius: 10px;
            border: 2px solid #005b8b;
        }

        button:hover {
            cursor: pointer;
            background-color: #005b8b;
        }

        input[type="radio"] {
            width: 20px;
            height: 20px;
            margin: -3px 16px 0px -33px;
            vertical-align: middle;
        }

        .radio-inline {
            margin-left: 10px;
        }

    </style>
</head>
<body>

<div class="container">
    <form method="post">
        <label for="name">Họ và tên:</label>
        <input type="text" id="name" name="name" required><br><br>

        <label for="gender">Giới tính: </label>
        <?php
        $genders = [
            0 => 'Nam',
            1 => 'Nữ',
        ];

        for ($i = 0; $i < count($genders); $i++) {
            echo '<label class="radio-inline">';
            echo "<input type='radio' class='form-control' name='gender' value='$i'>$genders[$i]</label>";
        }
        ?><br><br>

        <label for="department">Phân khoa:</label>
        <select id="department" name="department" required>
            <option value="">--Chọn phân khoa--</option>
            <?php
            $departments = [
                'MAT' => 'Khoa học máy tính',
                'KDL' => 'Khoa học vật liệu',
            ];

            foreach ($departments as $key => $value) {
                echo "<option value='$key'>$value</option>";
            }
            ?>
        </select><br><br>

        <button type="submit">Đăng ký</button>
    </form>
</div>

</body>
</html>

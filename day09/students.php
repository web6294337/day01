<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
    <style>
        body {
            font-family: Arial, sans-serif;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;

        }

        .container {
            background-color: white;
            text-align: left;
            border: 2px solid #005b8b;
            padding: 64px 45px 34px 35px;
            width: 190vh;
        }

        table {
            width: 100%;
            border-collapse: collapse;
        }

        th, td {
            border: 1px solid #868585;
            height: 25px;
            text-align: left;
            padding: 8px;
        }

        th {
            background-color: skyblue;
        }

        .action-buttons button {
            width: 42px;
            height: 23px;
            background-color: #92B1D6;
            color: white;
            border: 1px solid #4E6E96;
            text-align: center;
            cursor: pointer;
            margin-right: 5px;
        }

        .action-buttons button:hover {
            background-color: #4E6E96;
        }

        .col1 {
            width: 5%;
        }

        .col2 {
            width: 30%;
        }

        .col3 {
            width: 25%;
        }

        .col4 {
            width: 5%;
        }

        .button-container {
            display: flex;
            justify-content: center;
        }

        .flex {
            display: flex;
            justify-content: center;
            align-items: flex-start;
        }

        .felx-row {
            display: flex;
            justify-content: space-between;
        }

        label[for="department"],
        label[for="keyword"] {
            width: 96px;
            height: 35px;
            border: 2px solid #497B8D;
            color: white;
            background-color: #70AD47;
            text-align: center;
            line-height: 32px;
            margin: 10px;
        }

        select[name="department"],
        input[name="keyword"] {
            width: 265px;
            height: 32px;
            border: 2px solid #41719C;
            margin-left: 10px;
            padding: 2px;
            margin: 10px;
            background: #E1EAF4;
        }

        #reset, #seach, #button-add {
            width: 100px;
            height: 35px;
            justify-content: center;
            display: inline-block;
            margin: 10px;
            background-color: #4F81BD;
            border: 3px solid #385D8A;
            border-radius: 10px;
            text-align: center;
        }

        .test-flex-row {
            display: flex;
            flex-direction: column;
        }

        #confirmationModal {
            display: none;
            position: fixed;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            width: 300px;
            background-color: #fff;
            padding: 20px;
            border: 1px solid #ccc;
            box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
            z-index: 9999;
        }

        #confirmationModal p {
            margin-bottom: 15px;
        }

        #confirmDelete, #cancelDelete {
            padding: 10px 15px;
            margin-right: 10px;
            cursor: pointer;
        }

        #confirmDelete {
            background-color: #4CAF50;
            color: #fff;
        }

        #cancelDelete {
            background-color: #ccc;
            color: #000;
        }

    </style>
</head>

<body>

<div class="container">
    <div class="test-flex-row">
        <div class="flex">
            <label for="department">Khoa</label>
            <select id="department" name="department">
                <option value="">--Chọn phân khoa--</option>
                <?php
                $departments = [
                    'MAT' => 'Khoa học máy tính',
                    'KDL' => 'Khoa học vật liệu',
                ];

                foreach ($departments as $key => $value) {
                    echo "<option value='$key'>$value</option>";
                }
                ?>
            </select><br><br>
        </div>

        <div class="flex">
            <label for="keyword">Từ khóa</label>
            <input type="text" id="keyword" name="keyword" placeholder="Nhập từ khóa tìm kiếm"><br><br>
        </div>

        <div class="flex">
            <button id="reset">Reset</button>
        </div>
    </div>

    <div class="felx-row">
        <?php
        require 'database.php';

        $countSql = "SELECT COUNT(*) as total FROM students";
        $countResult = $conn->query($countSql);
        $row = $countResult->fetch_assoc();
        $totalStudents = $row['total'];

        echo "<strong>Số sinh viên tìm thấy: " . $totalStudents . "</strong>";

        $conn->close();
        ?>
        <button id="button-add" onclick='location.href= "register.php"'>Thêm</button>
    </div>

    <div>
        <table>
            <thead>
            <tr>
                <th class="col1">No</th>
                <th class="col2">Tên sinh viên</th>
                <th class="col3">Khoa</th>
                <th class="col4">Action</th>
            </tr>
            </thead>
            <tbody class="action-buttons">
            <?php
            require 'database.php';
            $sql = "SELECT id, name, department, birthdate FROM students";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                $i = 1;
                while ($row = $result->fetch_assoc()) {
                    echo "<tr>";
                    echo "<td>" . $i . "</td>";
                    echo "<td>" . $row["name"] . "</td>";
                    echo "<td>" . $row["department"] . "</td>";

                    echo "<td >";
                    echo "<div class='button-container'>";
                    echo "<button class='delete-btn' onclick=\"idToDelete('" . $row["id"] . "')\">Xóa</button>";
//                    echo "<button onclick='location.href=\"update_students.php\"'>Sửa</button>";
                    echo "<button onclick='location.href=\"update_students.php?id=" . $row["id"] . "\"'>Sửa</button>";
                    echo "</div>";
                    echo "</td>";

                    echo "</tr>";
                    $i++;
                }
            } else {
                echo "<tr><td colspan='4'>0 results</td></tr>";
            }
            $conn->close();
            ?>
            </tbody>
        </table>
    </div>
</div>

<!-- Delete Confirmation Modal -->
<div id="confirmationModal">
    <p>Bạn muốn xóa sinh viên này?</p>
    <button id="confirmDelete">Xóa</button>
    <button id="cancelDelete">Hủy</button>
</div>

<script>
    $(document).ready(function () {
        // Chuyển mảng $departments thành biến JavaScript
        var departments = <?php echo json_encode($departments); ?>;

        $("#reset").on("click", function () {
            $("#department").val("");
            $("#keyword").val("");
            resetTableData();
        });

        $("#department, #keyword").on("input", function () {
            searchAndPopulateTable();
        });

        function searchAndPopulateTable() {
            var departmentCode = $("#department").val();
            var keyword = $("#keyword").val();

            var departmentName = departments[departmentCode];

            $.ajax({
                url: "search.php",
                method: "POST",
                data: {department: departmentName, keyword: keyword},
                success: function (data) {
                    $(".action-buttons").html(data);
                }
            });
        }

        function resetTableData() {
            $.ajax({
                url: "reset.php",
                method: "POST",
                success: function (data) {
                    $(".action-buttons").html(data);
                }
            });
        }
    });

    function idToDelete(id) {
        $("#confirmDelete").off("click");
        $("#cancelDelete").off("click");

        $("#confirmationModal").css("display", "block");

        $("#confirmDelete").on("click", function () {
            $("#confirmDelete").off("click");
            $.ajax({
                url: "delete.php",
                method: "POST",
                data: {id: id},
                success: function (data) {
                    $(".action-buttons").html(data);
                }
            });

            $("#confirmationModal").css("display", "none");
        });

        $("#cancelDelete").on("click", function () {
            $("#confirmationModal").css("display", "none");
        });
    }

</script>

</body>

</html>

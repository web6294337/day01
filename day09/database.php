<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "ltweb";
$port = 3308;

$conn = new mysqli($servername, $username, $password, $database, $port);

// Kiểm tra kết nối
if ($conn->connect_error) {
    die("Kết nối thất bại: " . $conn->connect_error);
}
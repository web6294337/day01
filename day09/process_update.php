<?php
require 'database.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Nhận dữ liệu từ form
    $id = $_POST["id"];
    $name = $_POST["name"];
    $gender = ($_POST['gender'] == 0) ? 'Nam' : 'Nữ';
    $department = ($_POST['department']) == 'MAT' ? 'Khoa học máy tính' : 'Khoa học vật liệu';
    $birthdate = $_POST["birthdate"];
    $address = $_POST["address"];
    $img_path = $_POST["img"];

    // Cập nhật dữ liệu vào cơ sở dữ liệu
    $sql = "UPDATE students SET name='$name', gender='$gender', department='$department', birthdate='$birthdate', address='$address', img_path='$img_path' WHERE id=$id";

    if ($conn->query($sql) === TRUE) {
        header("Location: students.php"); // Chuyển về trang danh sách sinh viên
        exit();
    } else {
        echo "Lỗi cập nhật: " . $conn->error;
    }
} else {
    echo "Invalid request.";
}

// Đóng kết nối
$conn->close();
?>

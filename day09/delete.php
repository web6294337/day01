<?php
// delete.php
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["id"])) {
    // Include your database connection code
    require 'database.php';

    $id = $_POST["id"];

    // Perform the delete operation
    $deleteSql = "DELETE FROM students WHERE id = $id";
    if ($conn->query($deleteSql) === TRUE) {
//        header('location: students.php');
        echo "Student deleted successfully";
        // Redirect to students.php
        header("Location: students.php");
    } else {
        echo "Error deleting student: " . $conn->error;
    }

    // Close the database connection
    $conn->close();
} else {
    echo "Invalid request.";
}
$conn->close();
?>


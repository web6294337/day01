<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Đăng ký Tân sinh viên</title>
    <style>
        body {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
        }

        .container {
            display: inline;
            flex-direction: column;
            align-items: center;
            margin: 0 auto;
            background-color: white;
            text-align: left;
            border: 2px solid #005b8b;
            padding: 64px 45px 34px 35px;
        }

        label[for="name"],
        label[for="department"],
        label[for="gender"],
        label[for="birthdate"],
        label[for="address"],
        label[for="img"] {
            width: 96px;
            height: 35px;
            border: 2px solid #497B8D;
            display: inline-block;
            color: white;
            background-color: #70AD47;
            text-align: center;
            line-height: 32px;
        }

        input[name="name"],
        input[name="address"] {
            width: 265px;
            height: 32px;
            border: 2px solid #41719C;
            margin-left: 10px;
            padding: 2px;
        }

        input[name="img"] {
            width: 265px;
            height: 32px;
            margin-left: 10px;
            padding: 2px;
        }

        select[name="department"], input[name="birthdate"] {
            width: 145px;
            height: 36px;
            border: 2px solid #41719C;
            margin-left: 10px;
        }

        button {
            width: 130px;
            height: 43px;
            color: white;
            background-color: #70AD47;
            margin-top: 130px;
            margin-left: 130px;
            border-radius: 10px;
            border: 2px solid #005b8b;
        }

        button:hover {
            cursor: pointer;
            background-color: #005b8b;
        }

        input[type="radio"] {
            width: 20px;
            height: 20px;
            margin: -3px 16px 0px -33px;
            vertical-align: middle;
        }

        .data {
            display: inline-block;
            width: 265px;
            height: 32px;
            margin-left: 10px;
        }

        .avatar {
            width: 150px;
            height: 150px;
            object-fit: cover;
            border: 2px solid #fff;
            margin-left: 10px;
            transform: translateY(129px);
            margin-top: -133px;
        }

    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
</head>
<body>

<div class="container">
    <form method="post" action="process.php" enctype="multipart/form-data">
        <label for="name">Họ và tên</label>
        <div class="data"><?php echo $_POST['name']; ?></div>
        <br><br>

        <label for="gender">Giới tính</label>
        <div class="data"><?php echo $_POST['gender'] == 0 ? 'Nam' : 'Nữ'; ?></div>
        <br><br>

        <label for="department">Phân khoa</label>
        <div class="data">
            <?php
            $departments = [
                'MAT' => 'Khoa học máy tính',
                'KDL' => 'Khoa học vật liệu',
            ];
            echo $departments[$_POST['department']];
            ?>
        </div>
        <br><br>

        <label for="birthdate">Ngày sinh</label>
        <div class="data">
            <?php $formattedDate = date("d-m-Y", strtotime($_POST['birthdate']));
            echo $formattedDate; ?>
        </div>
        <br><br>

        <label for="address">Địa chỉ</label>
        <div class="data"><?php echo $_POST['address']; ?></div>
        <br><br>

        <label for="img">Hình ảnh</label>
        <div class="data">
            <?php
            $imgPath = 'uploads/' . $_FILES['img']['name'];
            move_uploaded_file($_FILES['img']['tmp_name'], $imgPath);
            echo '<img class = "avatar" src="' . $imgPath . '" alt="Hình ảnh">';
            ?>
        </div>
        <br><br>

        <input type="hidden" name="name" value="<?php echo $_POST['name']; ?>">
        <input type="hidden" name="gender" value="<?php echo ($_POST['gender'] == 0) ? 'Nam' : 'Nữ'; ?>">
        <input type="hidden" name="department" value="<?php echo $departments[$_POST['department']]; ?>">
        <input type="hidden" name="birthdate" value="<?php echo $_POST['birthdate']; ?>">
        <input type="hidden" name="address" value="<?php echo $_POST['address']; ?>">
        <input type="hidden" name="img" value="<?php echo $imgPath; ?>">

        <button type="submit" name="confirmButton" id="confirmButton">Xác nhận</button>

        <br><br>
    </form>
</div>


<!--<script>-->
<!--    $(document).ready(function () {-->
<!--        $("#confirmButton").click(function () {-->
<!--            alert("Đăng ký thành công!");-->
<!--        });-->
<!--    });-->
<!--</script>-->

</body>
</html>
